#!/usr/bin/env python3
''' Extract test cases info out of output.xml '''

import json

from robot.api import ExecutionResult, ResultVisitor


class TestCaseInfo(ResultVisitor):
    ''' Executed test cases info '''

    def __init__(self):
        self.test_cases_info = []

    def visit_test(self, test):
        self.test_cases_info.append({
            'name': test.name,
            'result': test.status,
            'tags': [str(tag) for tag in test.tags]
        })


def parse_output_xml():
    ''' Iterate through test cases '''
    result = ExecutionResult('/dev/stdin')
    visitor = TestCaseInfo()
    result.visit(visitor)
    return visitor.test_cases_info


if __name__ == '__main__':
    dictionaries = parse_output_xml()
    print(json.dumps(dictionaries, ensure_ascii=False))
